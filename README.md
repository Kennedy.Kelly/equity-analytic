# Equity Analytic

## Name
Equity Analytic

## Description
This repo contains everything required to pull benchmarks and create Equity Analytics. Report can be fully run and created from the 'Equity_Analytic_Report_Builder.R' file. 
If benchmarks do not exist for period, run pull_benchmarks in the Benchmark folder

## Installation
Version control built in 

## Support
kennedy.kelly@pressganey.com

## Roadmap
Ideas: combine to highest level service line and pull both race identification methods 
(For example: run at OU which will pull OU, ON, OY, etc and pull both patient self report and deirac_r - would require changing benchmarks too)

## Project status
Always being upgraded