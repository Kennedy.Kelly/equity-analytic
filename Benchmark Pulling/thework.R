


params_bm <- list(bm_type = bm_type,
                  sr= ifelse(self_report =='sr',T,F),
                  logkey=logkey)

params_pg <- list(service = inputService,
                  startDt = inputStart,
                  endDt = inputEnd,
                  no_rounding=T,
                  mode_info=F,
                  use_discharge_date=T
)




#if not self report, add in DEIRAC_R and DEIETH_E
if(!params_bm$sr){
  params_pg<- update_params(params_pg,demo_breakout = c('DEIRAC_R','DEIETH_E'))
}

if(params_bm$sr){
  params_pg <- update_params(params_pg,demo_breakout = c(lookup_px_items(filter_cahps_demo = F,
                                                                         service = params_pg$service) %>% 
                                                           filter(SurveyItemAboutYouFlag & SurveyItemCAHPSFlag) %>%
                                                           filter(tolower(SurveyItemName) %like% 'race|span|ethnici|hisp' &
                                                                    !tolower(SurveyItemReportName) %like% 'child') %>% 
                                                           .$SurveyItemVariableName))
  
  if(length(params_pg$demo_breakout)>=10){
    params_pg <- update_params(params_pg,demo_breakout = c(lookup_px_items(filter_cahps_demo = F,
                                                                           service = params_pg$service) %>% 
                                                             filter(SurveyItemAboutYouFlag & SurveyItemCAHPSFlag) %>%
                                                             filter(tolower(SurveyItemName) %like% 'race|span|ethnici|hisp' &
                                                                      !tolower(SurveyItemReportName) %like% 'child') %>% 
                                                             filter(SurveyItemVariableName %like% 'CL') %>% 
                                                             .$SurveyItemVariableName))
  }
  
}

if(bm_type == 'cms'){
  existing_bm <- params_pg$demo_breakout
  params_pg<- update_params(params_pg,demo_breakout = c(existing_bm,'ITCMS_SL'))
}

if(bm_type == 'item'){
  params_pg <- update_params(params_pg,client_breakout='client id')
}


if(bm_type == 'system'){
  params_pg <- update_params(params_pg,client_breakout = 'system id')
}


if(bm_type == 'facility'){
  
  params_pg <- update_params(params_pg,client_breakout = 'client id')
}

if(bm_type == 'site'){
  
  params_pg <- update_params(params_pg,entity_breakout = 'site')
  
}

if(bm_type == 'unit'){
  
  params_pg <- update_params(params_pg,entity_breakout = 'unit')
}


if(bm_type == 'unit type'){
  
  params_pg <- update_params(params_pg,entity_breakout = 'unit type')
}


scores <- do.call(pull_px_scores,params_pg )$items %>% 
  mutate(bm_type = bm_type,
         runKey = inputPeriodKey,
         startDt = inputStart,
         endDt = inputEnd)


if(params_bm$sr){
  #white non hispanic
  edwsk <- service_data[service_data$EDWSourceAbbreviation ==params_pg$service,'EDWSourceKey']
  
  
  demo_info <- lookup_px_items(filter_cahps_demo = F,
                               service= inputService)
  demo_info <- pg_query(glue("Select SurveyItemKey, SurveyItemReportName, 
                            SurveyItemVariableName from dbo.DimSurveyItem where 
                             SurveyItemVariableName in 
                             (" ,paste(sapply(params_pg$demo_breakout,
                                              function(x) paste0("'", x, "'")), 
                                       collapse = ","),") and EDWSourceKey = ", edwsk),'crdm') %>% 
    mutate(CleanCat = case_when(
      grepl('Hispanic', SurveyItemReportName) ~ 'Spanish, Hispanic, or Latino',
      grepl('White', SurveyItemReportName) ~ 'White',
      grepl('Alaska', SurveyItemReportName) ~ 'American Indian / Alaskan Native',
      grepl('Asia', SurveyItemReportName) ~ 'Asian',
      grepl('Hawa', SurveyItemReportName) ~ 'Hawaiian or Pacific Islander',
      grepl('Black', SurveyItemReportName) ~ 'Black or African American',
      grepl('Other', SurveyItemReportName) ~ 'Other')) 
  
  
  span_item <- demo_info[demo_info$SurveyItemReportName %like% 'Span|Hisp|Latin','SurveyItemVariableName']
  
  
  white_item <- demo_info[demo_info$SurveyItemReportName %like% 'White','SurveyItemVariableName']
  
  if(length(span_item)  >1 & length(white_item>1)){
    demo_info <- demo_info %>% filter(SurveyItemVariableName %like% 'CL')
  }
  if(length(span_item)>1){
    span_item <-  span_item[span_item %like% 'CL']
  }  
  
  #span hisp latino 
  outputdf <- data.frame()
  
  outputdf <- bind_rows(outputdf,scores[scores[[span_item]]  %like% 'YES|PUERTO|OTHER|MEX|CUBAN|ANOTHER ORIGIN' &
                                          scores[[white_item]] %like% 'YES|PUERTO|OTHER|MEX|CUBAN|ANOTHER ORIGIN',] %>% 
                          mutate(SelfReportedRace = 'White Hispanic') %>% 
                          group_by(across(any_of(c('SurveyItemKey','EDWSourceKey',   'ClientID', 'TopLevelClientID',
                                                   'ClientFacilityUnitKey','runKey','startDt','endDt','ITCMS_SL',
                                                   'SelfReportedRace')))) %>% 
                          summarize(TopBoxScore = weighted.mean(TopBoxScore, w =n),
                                    MeanScore = weighted.mean(MeanScore, w =n),
                                    demo_n = sum(n)) )
  
  
  outputdf <- bind_rows(outputdf,scores[(scores[[span_item]] =='NO' | scores[[span_item]] %like% 'NOT S') &
                                          scores[[white_item]] %like% 'YES|PUERTO|OTHER|MEX|CUBAN|ANOTHER ORIGIN',] %>% 
                          mutate(SelfReportedRace = 'White Non-Hispanic') %>% 
                          group_by(across(any_of(c('SurveyItemKey','EDWSourceKey','ITCMS_SL',   'ClientID', 'TopLevelClientID',
                                                   'ClientFacilityUnitKey','runKey','startDt','endDt',
                                                   'SelfReportedRace')))) %>% 
                          summarize(TopBoxScore = weighted.mean(TopBoxScore, w =n),
                                    MeanScore = weighted.mean(MeanScore, w =n),
                                    demo_n = sum(n)) )
  
  
  
  
  for(j in params_pg$demo_breakout){
    foc_info <- demo_info[demo_info$SurveyItemVariableName == j,]
    
    outputdf <- bind_rows(outputdf,
                          scores[scores[[foc_info$SurveyItemVariableName]]  
                                 %like% 'YES|PUERTO|OTHER|MEX|CUBAN|ANOTHER ORIGIN',] %>% 
                            mutate(SelfReportedRace = foc_info$CleanCat) %>% 
                            group_by(across(any_of(c('SurveyItemKey','EDWSourceKey', 'ITCMS_SL',  'ClientID', 'TopLevelClientID',
                                                     'ClientFacilityUnitKey','runKey','startDt','endDt',
                                                     'SelfReportedRace')))) %>% 
                            summarize(TopBoxScore = weighted.mean(TopBoxScore, w =n),
                                      MeanScore = weighted.mean(MeanScore, w =n),
                                      demo_n = sum(n)) )
    
    
    
  }
  
  
  if('ITCMS_SL' %in% names(outputdf)){
    
    outputdf <- outputdf %>% mutate(ITCMS_SL=case_when(ITCMS_SL ==1~'Maternity',
                                                       ITCMS_SL ==2~'Medical',
                                                       ITCMS_SL ==3~'Surgical',
                                                       T~NA_character_)) %>% filter(!is.na(ITCMS_SL))
  }
  scores <- outputdf %>% 
    mutate(bm_type = bm_type,
           runKey = inputPeriodKey,
           startDt = inputStart,
           endDt = inputEnd)
  
  remove(outputdf)
}

gc()

if(first_run & k==1){
  write_to_rasql(scores,table_name = 'Equity_Analytic_BM',  schema = 'dbo',
                 db = 'sandbox',
                 append = F,
                 overwrite=T)
  
  remove(scores)
}
if(!first_run | k>1){
  alldat <- bind_rows(pg_query('Select * from [RASandbox].[dbo].[Equity_Analytic_BM]', 'rasql'), scores)
  
  write_to_rasql(alldat,table_name = 'Equity_Analytic_BM',  schema = 'dbo',
                 db = 'sandbox',
                 append = T,
                 overwrite=F)
  
  #remove(alldat)
  remove(scores)
  
}
